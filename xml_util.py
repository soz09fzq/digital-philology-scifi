import re
import xml.etree.ElementTree as ET
import datetime

YEAR_MONTH_RE = re.compile(r"(?P<year>\d{4}).(?P<month>\d{2})")
MONTH_WORD_YEAR_RE = re.compile(r"(?P<month>\w+).(?P<year>\d{4})")
YEAR_MONTH_WORD_RE = re.compile(r"(?P<year>\d{4}).(?P<month>\w+)")

def sub_element_text(parent, tag, text = ""):
    """Create a xml sub-element with the given tag and text and return this xml element."""
    e = ET.SubElement(parent, tag)
    e.text = text
    return e

def extract_id(meta_xml: ET.Element):
    """Extracts the archive.org id from meta.xml."""
    root = meta_xml.getroot()
    return root.find("identifier-ark").text

def extract_date(meta_xml: ET.Element):
    """
    **Tries** to extract a date from meta.xml.
    Currently a simple regex search is done over specific tags like 'date, title, description' to identify the myriads possible combinations of date representations.
    Not all meta.xml contain dates.
    Logially, an extraction for all meta.xml's is not possible and an exception will raise.
    """
    tags = ["date", "title", "identifier", "description"]
    regs = [YEAR_MONTH_RE, MONTH_WORD_YEAR_RE, YEAR_MONTH_WORD_RE]
    
    root = meta_xml.getroot()
    result = None
    for tag, reg in zip(tags, regs):
        date = root.find(tag)
        if date is None:
            continue
        else:
            date = date.text
        result = reg.search(date)
        if result is not None:
            month = result.group("month")
            year = result.group("year")
            date = year + "-" + month
            try:
                return datetime.datetime.strptime(date, "%Y-%m")
            except:
                return datetime.datetime.strptime(date, "%Y-%B")

    raise AttributeError(f"No date found in title {root.find('title').text}")

def build_xml(id, name, pubdate, toc, issue_date=None, issue=-1, publisher="", editor="", isbn="", front_image=""):
    """
    Creates a xml with the given metadata.
    id, name, pubdate and the toc are mandatory, the rest of the args are optional.
    """
    data = ET.Element("magazine")
    sub_element_text(data, "magazine-id", id)
    sub_element_text(data, "magazine-name", name)
    date = sub_element_text(data, "magazine-pubdate", pubdate)

    # optionals
    if issue_date is not None:
        date.set("issue-date", issue_date.strftime("%Y-%m"))
    if len(publisher) > 0:
        sub_element_text(data, "magazine-publisher", publisher)
    if len(editor) > 0:
        sub_element_text(data, "magazine-editor", editor)
    if issue != -1:
        sub_element_text(data, "magazine-issue", str(issue))
    if len(isbn) > 0:
        sub_element_text(data, "magazine-isbn", isbn)
    if len(front_image) > 0:
        sub_element_text(data, "magazine-frontimage", front_image)

    xml_toc = ET.SubElement(data, "toc")
    for title, author, page, type in toc:
        if author is None:
            author = ""
        
        try:
            page = int(float(page))
        except:
            # page is nan, so we leave this attribute empty
            page = ""
            
        attr = {
            "author": str(author),
            "page": str(page),
            "type": str(type)
        }
        
        entry = ET.SubElement(xml_toc, "toc-entry", attr)
        entry.text = title

    return data


def extract(issue_date, filter, mag_id):
    pub_title = str(filter["pub_title"].iat[0])
    pub_year = filter["pub_year"].iat[0]
    publisher = str(filter["publisher_name"].iat[0])
    editor = str(filter["editor_name"].iat[0])
    img = str(filter["pub_frontimage"].iat[0])

    toc = filter[["title_title", "author_name",
                          "pubc_page", "title_ttype"]].to_numpy()

    xml = build_xml(mag_id, pub_title, pub_year, toc,
                             issue_date=issue_date, editor=editor, publisher=publisher, front_image=img)
                     
    return xml