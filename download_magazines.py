import internetarchive
import argparse
import os
from multiprocessing import cpu_count
from multiprocessing.pool import Pool

FILE_GLOB = "*.xml"
SCI_FI_COLLECTIONS = [
    #"warren-1984-magazine",
    #"amazingstoriesmagazine",
    "asimovmagazine",
    "astoundingstories",
    "authenticsciencefiction",
    #"fantasticsfstories",
    #"fantasyandsciencefiction",
    "galaxymagazine",
    "ifmagazine",
    #"interzonemagazine",
    #"pulp_misc_sf",
    #"newworldssf",
    #"planetstories",
    #"sciencefantasy",
    #"sciencefictionquarterly",
    #"sciencefictionstories",
    #"sciencewonderstories",
    #"starburstmagazine",
    #"startling_stories",
    #"thrustsfmagazine",
    ]


def download(args):
    """
    downloads an internet archive item.
    """

    # TODO: requests to internet archive seem to be very slow but download speeds are okay.
    # Every magazine/item has a .torrent file. It certainly would be faster to download all torrent files and download the rest of the files via torrent.
    identifier, dest_dir, file_glob = args
    print("Identifier:", identifier)
    internetarchive.download(identifier, destdir=dest_dir,
                             verbose=False, checksum=True, glob_pattern=file_glob)
    return os.path.join(dest_dir, identifier)


def cli_args():
    p = argparse.ArgumentParser(description=__doc__,
                                formatter_class=argparse.RawDescriptionHelpFormatter)

    p.add_argument("-d", "--dest_dir",
                   default="scifi", help="download directory. Default: './scifi'")
    p.add_argument("-i", "--identifier", type=str,
                   help="internet archive identifier when you want to download something specific")
    p.add_argument('-j', '--threads', help="Number of thread to use for download", default=0, dest="threads", type=int)

    return (p.parse_args())

def parallel_download(urls, cpuCount=0):
    if cpuCount == 0:
        cpuCount = cpu_count() - 1
    results = Pool(cpuCount).imap_unordered(download, urls)
    for r in results:
        print("Result:", r)

if __name__ == "__main__":
    args = cli_args()
    if args.identifier is None:
        for collection_id in SCI_FI_COLLECTIONS:
            search = internetarchive.search_items(collection_id)
            urls = []
            for s in search:
                magazine_id = s["identifier"]
                print("Magazine_id: ", magazine_id)
                dest_dir = os.path.join(args.dest_dir, collection_id)
                if not os.path.exists(dest_dir):
                    os.makedirs(dest_dir)
                urls.append((magazine_id, dest_dir, FILE_GLOB))
            parallel_download(urls, cpuCount=args.threads)
    else:
        download(args.identifier, args.dest_dir, FILE_GLOB)
