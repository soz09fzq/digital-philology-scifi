import xml.etree.ElementTree as ET
from xml.dom import minidom
import sys
import os
import re
import itertools
import numpy as np
import matplotlib.pyplot as plt
from contextlib import contextmanager
import json
from multiprocessing import cpu_count
from multiprocessing.pool import Pool
import spacy
from spacy import displacy
import sqlite3 as sql
import en_core_web_sm
import string
import pandas as pd
from tqdm import tqdm
import csv
import argparse
from functools import partial


def computeWordFontSize(word):
    coords = [int(c) for c in word.attrib['coords'].split(',')]
    return coords[1] - coords[3], (coords[2] - coords[0]) / len(word.text)
    
def page2str(page):
    return " ".join(w.text for w in page.findall(".//WORD"))

def findTOC(fname):
    try:
        tree = ET.parse(open(fname))

        dPages = [o for o in tree.getroot().findall(".//OBJECT") if 'type' in o.attrib and o.attrib['type'] == 'image/x.djvu']

        #print(dPages)
        numPattern = re.compile(r'[0-9]+')

        numCounts = []
        for p in dPages:
            words = p.findall('.//WORD')
            numCount = 0
            if len(words) < 1:
                continue
            for w in words:
                if numPattern.fullmatch(w.text):
                    numCount += 1
            if len(words) < 10:
                numCounts.append(0.0)
            else:
                numCounts.append(numCount / len(words))
            #print(numCount / len(words))

        numberyness = np.array(numCounts)
        avgNumberyness = np.mean(numberyness)
        stdNumberyness = np.std(numberyness)
        #print(avgNumberyness, stdNumberyness)
        # plt.plot(numberyness)
        # plt.show()

        # for i, (p, n) in enumerate(zip(dPages, numberyness)):
        #     dev = (n - avgNumberyness) / stdNumberyness
        #     if dev >= 2.0:
        #         print(i, page2str(p), n)

        probableTocPageNum = 1+np.argmax(numberyness[1:int(0.2 * len(numberyness))])

        probableTocPage = dPages[probableTocPageNum]

        #print(f"file: {fname} TOC probably on page {probableTocPageNum}/{len(dPages)}")
        #print(" ".join([w.text for w in words]))

        dLines = probableTocPage.findall('.//LINE')

        lines = []
        words = []

        for line in dLines:
            ws = [w for w in line.findall("WORD")]
            l = [w.text for w in ws]
            lines.append((' '.join(l), line))
            words.extend(ws)

        patterns = [
            re.compile(r"[0-9]+\s.+"),
            # re.compile(r"is\s.+"),  # 15
            # re.compile(r"si\s.+"),   # 51
            re.compile(r".+\s[0-9]+"),
            # re.compile(r".+\sis"),  # 15
            # re.compile(r".+\ssi")   # 51
        ]

        vsizes = []
        hsizes = []
        for w in words:
            vSize, hSize = computeWordFontSize(w)
            vsizes.append(vSize)
            hsizes.append(hSize)

        tocLines = []

        tocExcludes = [
            "All rights reserved",
            "NY 10017",
            "Novellas",
            "Novelettes",
            "Short Stories",
            "Departments",
            "Poetry",
            "Dell Magazines"
        ]

        for p, (i, (l, data)) in itertools.product(patterns, enumerate(lines)):
            m = p.fullmatch(l)
            if m:
                tocLines.append((i,l))

        rm = set()
        for j, (i, l) in enumerate(tocLines):
            for e in tocExcludes:
                if e in l:
                    rm.add(j)

        for v in rm:
            tocLines.pop(v)

        tocLineNums = sorted([i for i, _ in tocLines])

        for i, j in enumerate(tocLineNums[1:]):
            if j - tocLineNums[i] > 1:
                for k in range(tocLineNums[i]+1, j):
                    isTitle = False
                    for e in tocExcludes:
                        if e in lines[k][0]:
                            isTitle = True
                            break
                    if isTitle:
                        break
                    tocLines[i] = (j, tocLines[i][1] + "\n" + lines[k][0])

        #for l in tocLines:
        #    print(l)
        return tocLines, probableTocPageNum, fname
    except:
        return [], -1, fname

@contextmanager
def pushd(new_dir):
    """Temporarily change to another directory."""
    prev = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(prev)


def find(path, patterns=None):
    """
    Find files matching given patterns as regex.\n
    Searches recursively through directories.
    """
    results = set()
    with pushd(path):
        for dirpath, dirs, files in os.walk('.'):
            for file in files:
                filePath = os.path.join(dirpath, file)
                if patterns is None:
                    yield filePath
                for p in patterns:
                    if re.match(p, filePath):
                        yield os.path.abspath(filePath)

def writeMetaXML(df, ner, fname, date, issue, page, lines, magazineName=""):
    dest_path = os.path.join(os.path.abspath('.'), 'data', 'ocr-toc', os.path.relpath(os.path.dirname(fname), os.path.abspath('./scifi')))
    os.makedirs(dest_path, exist_ok=True)
    root = ET.Element("root")


    ET.SubElement(root, "magazine-pubdate").text = str(date)
    ET.SubElement(root, "magazine-issue").text = str(issue)
    ET.SubElement(root, "magazine-id").text = magazineName

    foundData = []

    toc = ET.SubElement(root, "toc", page=str(page))
    for num, text in lines:
        cleaned = text.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))
        attributes = {
            'page': "0",
            'author': '',
        }
        for w in ner(text).ents:
            if w.label_ == 'PERSON':
                attributes['author'] = w.text
                cleaned = cleaned.replace(w.text, '')
            elif w.label_ == 'CARDINAL':
                attributes['page'] = re.sub("\s+", '', w.text)
                cleaned = cleaned.replace(w.text, '')
        for w in ner(cleaned).ents:
            if w.label_ == 'PERSON':
                if len(attributes['author']):
                    attributes['author'] += ", " + w.text
                else:
                    attributes['author'] = w.text
                cleaned = cleaned.replace(w.text, '')
            elif w.label_ == 'CARDINAL':
                attributes['page'] = re.sub("\s+", '', w.text)
                cleaned = cleaned.replace(w.text, '')
        title = re.sub("\s+", ' ', cleaned.strip())
        #resp = df.query("title_title == '%s'" % title.lower())
        #if len(resp) >= 1:
        #ids = [str(r['title_id']) for _, r in resp.iterrows()]
        #attributes['title_id'] = ','.join(ids)

        if 'by' in title.lower():
            ls = title.split('by')
            title = ls[0].strip()
            attributes['author'] += 'and'.join(ls[1:])

        ET.SubElement(toc, "toc-entry", attrib=attributes).text = title
        attributes['title'] = title
        try:
            attributes['page'] = int(attributes['page'])
        except:
            attributes['page'] = -1
        foundData.append(attributes)

    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent='  ')
    with open(os.path.join(dest_path, "meta.xml"), "w") as f:
        f.write(xmlstr)

    foundData.sort(key=lambda x : x['page'])
    
    for i, e in enumerate(foundData[:-1]):
        e['endPage'] = foundData[i+1]['page']
        e['file'] = fname

    if len(foundData) > 0:
        foundData[-1]['endPage'] = -1
        foundData[-1]['file'] = fname

    return foundData

def processSingleFile(args):
    file, metaFile = args
    metaTree = ET.parse(open(metaFile))
    metaRoot = metaTree.getroot()
    ark_id = metaRoot.findall(".//identifier-ark")[0].text
    #print(ark_id, metaFile)
    ner = en_core_web_sm.load()
    dateRegex = re.compile("\d{4}-\d{2}")
    issueRegex = re.compile(r"(\d{1,3})[_\-]19\d{2}")
    lines, p, fname = findTOC(file)
    date = re.findall(dateRegex, fname)
    if len(date) >= 1:
        date = date[0]
    else:
        date = None
    issue = re.findall(issueRegex, fname)
    if len(issue) >= 1:
        issue = int(issue[0])
    else:
        issue = -1
    
    entries = writeMetaXML(None, ner, fname, date, issue, int(p), lines, ark_id)
    for e in entries:
        writer.writerow(e)
    csvFile.flush()
    return entries

def findFilesAnMeta(path):
    djvu_regex = re.compile(".*_djvu\.xml")
    meta_regex = re.compile(".*_meta\.xml")
    djvu_files = []
    meta_files = []
    for root, dirs, files in os.walk(path):
        mFile = None
        dFile = None
        for f in files:
            fullname = os.path.join(root, f)
            if re.match(djvu_regex, f):
                dFile = fullname
            elif re.match(meta_regex, f):
                mFile = fullname
        if mFile and dFile:
            djvu_files.append(dFile)
            meta_files.append(mFile)
    return zip(djvu_files, meta_files)

def writeTOCIndex(path, db_path="isfdb/isfdb.db", split_data_path="splits.csv", cpuCount=12):
    tocData = {}
    dateRegex = re.compile("\d{4}-\d{2}")
    issueRegex = re.compile(r"(\d{1,3})[_\-]19\d{2}")

    conn = sql.connect(db_path)

    output_path = os.path.join(os.path.abspath('.'), 'data', 'ocr-toc')
    os.makedirs(output_path, exist_ok=True)

    # print("Loading database table into memory.")
    # df = pd.read_sql_query("SELECT * FROM titles;", conn)
    # print("Converting titles to lowercase.")
    # df['title_title'] = [t.lower() for t in df['title_title']]

    if os.path.isdir(path):
        print(f"Using {cpuCount} threads.")
        # files = [f for f in find(path, [re.compile(".*_djvu\.xml")])]
        # metaFiles = [f for f in find(path, [re.compile(".*_meta\.xml")])]
        files = [x for x in findFilesAnMeta(path)]
        print(f"Found {len(files)} ocr-files.")
        results = Pool(cpuCount).imap(processSingleFile, files)
        for e in tqdm(results, total=len(files)):
            pass
            #try:
            
            # except Exception as e:
            #     print(e)
            #     pass
    else:
        lines, p, fname = findTOC(path)
        date = re.findall(dateRegex, fname)
        if len(date) >= 1:
            date = date[0]
        else:
            date = None
        issue = re.findall(issueRegex, fname)
        if len(issue) >= 1:
            issue = int(issue[0])
        else:
            issue = -1
        entries = writeMetaXML(df, ner, fname, date, issue, int(p), lines, "")
        for e in entries:
            writer.writerow(e)
    with open("toc.json", 'w') as f:
        f.write(json.dumps(tocData, indent=2))
    csvFile.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser("find_toc")
    parser.add_argument("path", help="The path to search for files. If file use only this file.")
    #parser.add_argument("-d", "--database", help="The path to the database to use.", dest="db_path", default="isfdb/isfdb.db")
    parser.add_argument("-o", "--output", help="File use for split data output.", dest="split_data_path", default="splits.csv")
    parser.add_argument("-j", "--cpu-count", help="Number of CPU cores to use.", dest="cpu_count", default=8, type=int)

    data = parser.parse_args()
    outTable = {
        "file" : [],
        "title": [],
        "author": [],
        "page": [],
        "endPage": [],
        "title_id": [],
    }
    csvFile = open(data.split_data_path, "w")
    writer = csv.DictWriter(csvFile, fieldnames=list(outTable.keys()))
    writer.writeheader()
    writeTOCIndex(data.path, "", data.split_data_path, data.cpu_count)
    df = pd.read_csv(data.split_data_path)
    print(df)
