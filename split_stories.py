import argparse
import os
import xml.etree.ElementTree as ET
from xml.dom import minidom
import pandas as pd
from tqdm import tqdm
import math


def splitFiles(splitData, indexFile, dbOnly=True):
    fileNames = set(splitData['file'])
    
    root = ET.Element("root")
    for fname in tqdm(fileNames):
        rows = splitData.loc[splitData['file'] == fname]
        
        tree = ET.parse(open(fname))
        dPages = [o for o in tree.getroot().findall(".//OBJECT") if 'type' in o.attrib and o.attrib['type'] == 'image/x.djvu']

        for i, r in rows.iterrows():
            if dbOnly and isinstance(r['title_id'], float) and math.isnan(r['title_id']):
                continue
            if int(r['endPage']) > 0:
                pages = dPages[int(r['page']):int(r['endPage'])+1]
            else:
                pages = dPages[int(r['page']):int(r['endPage'])]
            lines = []
            for p in pages:
                for l in p.findall(".//LINE"):
                    lines.append(" ".join([w.text for w in l.findall(".//WORD")]))
            text = "\n".join(lines)
            outFname = ""
            if not isinstance(r['title'], str):
                outFname = os.path.join(os.path.dirname(fname), str(r['title']).replace(" ", '_') + ".txt")
            else:
                if len(r['title']) < 32:
                    outFname = os.path.join(os.path.dirname(fname), r['title'].replace(" ", '_') + ".txt")
                else:
                    outFname = os.path.join(os.path.dirname(fname), r['title'][:32].replace(" ", '_') + ".txt")
            with open(outFname, "w") as f:
                f.write(text)
            ET.SubElement(root, "story", title=str(r['title']), author=str(r['author']), file=outFname)
    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent='  ')
    with open(indexFile, "w") as f:
        f.write(xmlstr)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("split_stories")
    parser.add_argument("split_file", help="File storing document splits.")
    parser.add_argument("-o", "--output", help="Output file with index to stories.", dest="out_fname", default="index.xml")
    parser.add_argument("-d", "--require-db-entry", help="When set a database entry is required for an entry to be considered valid.", default=False, action="store_true", dest="db_req")

    data = parser.parse_args()
    df = pd.read_csv(data.split_file)
    print(df)
    splitFiles(df, data.out_fname, dbOnly=data.db_req)
