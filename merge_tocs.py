import os
import argparse
import re
import xml.etree.ElementTree as ET
from xml.dom import minidom
from dataclasses import dataclass
from typing import List

@dataclass
class TocEntry:
    page: int
    author: str
    type: str | None
    title: str
    source: List[str]

@dataclass
class Magazine:
    ark_id: str
    pub_date: str | None
    issue: str | None
    name: str | None
    toc: List[TocEntry]
    source: List[str]

def mergeTocs(toc_a: List[TocEntry], toc_b : List[TocEntry]) -> List[TocEntry]:
    res = []
    # for a in toc_a:
    #     for b in toc_b:
    #         if a == b:
    #             res.append(a)
    #             continue
    #     res.append(a)
    # for b in toc_b:
    #     for a in toc_a:
    #         if a != b:
    #             continue
    #     res.append(b)
    return toc_a + toc_b

def mergeMagazines(mag_a: Magazine, mag_b: Magazine) -> Magazine:
    return Magazine(mag_a.ark_id, mag_a.pub_date, mag_a.issue, mag_a.name, mergeTocs(mag_a.toc, mag_b.toc), mag_a.source + mag_b.source)

def readTocEntries(tocTag : ET.Element, source: str) -> List[TocEntry]:
    entries = tocTag.findall('.//toc-entry')
    res = []
    for e in entries:
        try:
            page = int(e.attrib['page'])
        except ValueError:
            page = -1
        author = e.attrib['author']
        title = e.text
        type = None
        if 'type' in e.attrib.keys():
            type = e.attrib['type']
        res.append(TocEntry(page, author, type, title, [source]))
    return res

def parseMetaXMLToClass(fname: str) -> Magazine:
    source = os.path.basename(os.path.dirname(os.path.dirname(os.path.dirname(fname))))
    tree = ET.parse(open(fname))
    root = tree.getroot()
    try:
        ark_id = root.find('magazine-id').text
    except AttributeError:
        ark_id = None
    pub_date = root.find('magazine-pubdate').text
    try:
        name = root.find('magazine-name').text
    except AttributeError:
        name = ""
    toc = readTocEntries(root.find('toc'), source)
    return Magazine(ark_id, pub_date, None, name, toc, [source])

def writeTocEntry(toc: ET.Element, entry: TocEntry):
    attrib = {
        "page": str(entry.page),
        "author": str(entry.author),
        "type": str(entry.type),
        "source": ','.join(entry.source),
    }
    ET.SubElement(toc, 'toc-entry', attrib).text = entry.title


def writeMagazineToXML(mag: Magazine, source_path: List[str]):
    root = ET.Element('root')
    
    ET.SubElement(root, "magazine-id").text = mag.ark_id
    ET.SubElement(root, "magazine-issue").text = mag.issue
    ET.SubElement(root, "magazine-pubdate").text = mag.pub_date
    ET.SubElement(root, "magazine-source-path").text = "\n".join(source_path)
    ET.SubElement(root, "magazine-data-sources").text = ",".join(mag.source)
    toc = ET.SubElement(root, "toc")

    for e in mag.toc:
        writeTocEntry(toc, e)

    return root


def getArkIdFromFile(fname):
    tree = ET.parse(open(fname))
    root = tree.getroot()
    candidates = root.findall('.//magazine-id')
    if len(candidates) > 0:
        return candidates[0].text
    return None

def collectFilesByArkId(path, regex=re.compile(".*\.xml")):
    collected = {}
    for root, dirs, files in os.walk(path):
        for f in files:
            fullPath = os.path.join(root, f)
            if re.match(regex, f):
                magazine = parseMetaXMLToClass(fullPath)
                if magazine.ark_id is None:
                    continue
                if magazine.ark_id not in collected.keys():
                    collected[magazine.ark_id] = []
                collected[magazine.ark_id].append((magazine, fullPath))
    return collected

def id_to_path(ark_id: str) -> str:
    return ark_id.replace("ark:/", "ark_").replace("/", '_')

def mergeFiles(files, ark_id):
    output_dir = os.path.join('merged', id_to_path(ark_id))
    os.makedirs(output_dir, exist_ok=True)
    output_file = os.path.join(output_dir, "meta.xml")
    mag, source_path = files[0]
    source_files = [source_path]
    for fmag, fpath in files[1:]:
        mag = mergeMagazines(mag, fmag)
        source_files.append(fpath)
    
    root = writeMagazineToXML(mag, source_files)
    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent='  ')
    with open(output_file, "w") as f:
        f.write(xmlstr)

if __name__ == '__main__':
    parser = argparse.ArgumentParser("merge_tocs")
    parser.add_argument("path", help="The path to search for meta files.")

    data = parser.parse_args()
    collectedFiles = collectFilesByArkId(data.path)
    print(collectedFiles.keys())
    for k, files in collectedFiles.items():
        mergeFiles(files, k)