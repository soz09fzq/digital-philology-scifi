import typer
import glob
import os

app = typer.Typer()


@app.command()
def remove_metadata(corpus_path: str):
    metas = glob.glob("**/*/metadata.xml", recursive=True)
    for meta in metas:
        os.remove(meta)
    
    print(f"Deleted {len(metas)} metadata.xml")        
    
if __name__ == "__main__":
    app()