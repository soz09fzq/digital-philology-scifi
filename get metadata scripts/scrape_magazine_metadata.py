from lxml import etree as ET
from bs4 import BeautifulSoup
import re, sys, os, requests



def get_xml_metadata(xml_root: ET.Element, xml_tag: str) -> list:
    """Extracts and returns value of given xml tag from xml element."""

    return xml_root.findall(xml_tag)[0].text



def get_isfdb_link(metadata_internet_archive: ET._ElementTree) -> str:
    """
    Attempts to parse and extract only the isfdb link from the given xml tree containing metadata of the magazine issue.
    
    :returns link: url of web page containing the magazine issue's metadata: author, text titles, starting page number, author page url and text page url
    """

    for meta in metadata_internet_archive.findall('description'):  #  content of description is html; contains link to scifi-archive website; get metadata (inkluding page number) from there

        soup = BeautifulSoup(meta.text, features="html.parser")
        try:
            link = soup.find('a').get("href")  #  scifi-archive page for this edition of the magazine
        except AttributeError as err:
            return None
    
    return link


def scrape_issue_metadata_from_isfdb(url: str, xml_root: ET._Element) -> None:

    """
    Every magazine issue page contains a 'Contents' block with metadata (authors, texts featured in the issue, along with page numbers).
    This function extracts magazine issue metadata from there.
    """

    #  create new xml tree:
    new_metadata = ET.Element("root")

    #  add needed nodes to xml tree:
    mag_id = ET.SubElement(new_metadata, "magazine-id")
    mag_name = ET.SubElement(new_metadata, "magazine-name")
    mag_pub_date = ET.SubElement(new_metadata, "magazine-pubdate")
    toc = ET.SubElement(new_metadata, "toc")


    #  add values to nodes (mag_id, mag_name) from internet archives metadata
    mag_id.text = get_xml_metadata(xml_root, 'identifier-ark')
    mag_name.text = get_xml_metadata(xml_root, 'title')


    #  add values to nodes (mag_pub_date, toc) from isfdb.org:
    issue_metadata_div = str(BeautifulSoup(requests.get(url).text,"html.parser").find_all("div", {"class": "ContentBox"})[0]) 
    mag_pub_date.text = issue_metadata_div.split("<b>Date:</b>")[1].split("<li>")[0].strip()


    table_of_contents_div = BeautifulSoup(requests.get(url).text,"html.parser").find_all("div", {"class": "ContentBox"})[1]  #  there are two div elements of class content boxes; 1st one: meta about mag; 2nd one: contents of mag


    #  toc is made up of html list entities:
    lst_elements = table_of_contents_div.find("ul")
    content_list = str(lst_elements).split("<li>")

    print(f" ({len(content_list[1:])} entries):")

    for index, magazine_entry in enumerate(content_list[1:]):  #  1st element is <ul> tag, skip it


        print(f"{index+1}", end="")


        toc_entry = ET.SubElement(toc, "toc-entry", page="", author="", type= "")

        page_number, rest = magazine_entry.split("•", 1)
        page_number = re.sub("[^0-9]", "", page_number)
 

        if (page_number.isnumeric() and len(page_number) < 4) or not page_number:  #  check for potentially wrong extraction of page number; 'fep' or 'bep' become empty page numbers
            toc_entry.attrib['page'] = page_number
        else:
            print("     Extracted faulty page number. Leave empty page number, author and type.")
            continue


        try:
            title, rest = rest.split("•", 1)
        except ValueError as e:
            print("     Error. TOC not in correct format. Leave empty author and type.")
            continue
            

        soup = BeautifulSoup(title, features="html.parser")
        toc_entry.text = soup.find('a').text
        toc_entry.attrib['type'] = scrape_text_type(soup.find('a').get("href"))
        
        additional_info_in_squared_brackets = re.findall(r'\[.*?\]', rest)  #  sometimes there is additional info in parantheses/brackets; remove for easier handling
        additional_info_in_parantheses = re.findall(r'\(.*?\)', rest)

        for elem in additional_info_in_squared_brackets:
            rest = rest.replace(elem, '')

        for elem in additional_info_in_parantheses:
            rest = rest.replace(elem, '')

        
        soup = BeautifulSoup(rest, features="html.parser")
        author_search_list = soup.find_all('a')  

        authors_found = []     

        try:
            for author_html in author_search_list:
                authors_found.append(BeautifulSoup(str(author_html), features="html.parser").find('a').text)   
 
        except AttributeError as err:
            authors_found.append('')


        toc_entry.attrib['author'] = ", ".join(authors_found)
        print("\b\b", end="")

    
    return new_metadata

        


def scrape_text_type(text_url: str) -> str:
    """
    Scrapes and returns text type from specified text page on isfdb.
    (For ex. 'ESSAY' or 'NOVELLA')
    """

    text_meta_div = BeautifulSoup(requests.get(text_url).text,"html.parser").find_all("div", {"class": "ContentBox"})[0].text  #  there are two div elements of class content boxes; 1st one: meta about mag; 2nd one: contents of mag
    
    return str(text_meta_div).split("Type:")[1].split("\n")[0].strip()



def get_all_issues(root: str) -> list:

    """Returns a list of all files ending on "*_meta.xml" in subdirectories of given root."""

    meta_files = []

    for path, subdirs, files in os.walk(root):
        for filename in files:
            if filename.endswith("_meta.xml"):
                meta_files.append(path + "\\" + filename)


    return meta_files


def generateXML(xml_content, issue_xml_path: str = "") -> None:

        path = '\\'.join(issue_xml_path.rsplit("\\")[:-1])
        tree = ET.ElementTree(xml_content)
        tree.write(path+"\\scraped_metadata.xml", pretty_print=True)

def generate_magazine_metadata(root: str) -> None:

    """
    root should be either the root folder of one specific magazine issue (for ex.: 'scifi\\ifmagazine\\1974-02_IF') or
    the root of all magazine issues, to extract metadata for all issues in the magazine corpus at once (for ex.: scifi\\ifmagazine')

    """

    xml_paths = get_all_issues(root)


    for index, issue_xml_path in enumerate(xml_paths):

        print(f"processing issue {index+1} of {len(xml_paths)}", end=(""))

        issue_link = get_isfdb_link(ET.parse(issue_xml_path))

        if issue_link:
            generateXML(scrape_issue_metadata_from_isfdb(issue_link, ET.parse(issue_xml_path).getroot()), issue_xml_path)

        else:
            print(f"Couldn't find isfdb link for: {issue_xml_path}")





if __name__ == "__main__":

    generate_magazine_metadata('scifi\\galaxymagazine')  
    #generate_magazine_metadata('scifi\\ifmagazine')  


