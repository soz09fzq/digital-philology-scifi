#!/usr/bin/env bash

# internet archive collections for our corpora
COLLECTIONS=("galaxymagazine")

# check if parallel is installed
if ! command -v parallel &> /dev/null
then
    echo "Parallel could not be found. Please install parallel."
    echo "For more information: https://www.gnu.org/software/parallel/"
    exit
fi


# download internet archive binary
if [ ! -f ia ]; then
    echo "Downloading internet archive binary."
    curl -LOs https://archive.org/download/ia-pex/ia
    chmod +x ia
fi

# download all collections from COLLECTIONS
# to speed up downloading we do requests in parallel
for collection in ${COLLECTIONS[@]}; do
    echo "Downloading collection $collection"
    mkdir -p scifi/$collection
    export collection # workaround for passing variables to parallel. See: https://stackoverflow.com/questions/12100136/inheriting-environment-variables-with-gnu-parallel
    ./ia search $collection --itemlist | parallel -j 0 './ia download -C {} --glob="*_dvju.xml" --destdir=scifi/$collection'   
done

# remove internet archive binary
rm ia
