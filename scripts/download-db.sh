# install gdown
# pip install gdown

mkdir isfdb
cd isfdb
# download mysql database dump
gdown 1ExYdllo3ss4BO1GnAyhnvi-6KOuEqYd1 # https://www.isfdb.org/wiki/index.php/ISFDB_Downloads - Date: 2022-12-31
unzip *.zip

wget "https://raw.githubusercontent.com/dumblob/mysql2sqlite/master/mysql2sqlite"
chmod +x mysql2sqlite
./mysql2sqlite -O cygdrive/c/ISFDB/Backups/* | sqlite3 isfdb.db