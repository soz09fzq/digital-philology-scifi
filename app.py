import glob
import json
import os
import xml.etree.ElementTree as ET
from functools import partial
from multiprocessing import Pool
from pathlib import Path

import pandas as pd
import typer
from rich import print
from rich.progress import Progress, SpinnerColumn, TextColumn
from thefuzz import fuzz

from db_util import *
from xml_util import *

app = typer.Typer()


def process(conn, magazine_path, pub_title=None, pub_tag=None):
    with Progress(
        SpinnerColumn(),
        TextColumn("[progress.description]{task.description}"),
        transient=True,
    ) as progress:
        progress.add_task(
            description=f"Processing db query for {magazine_path}", total=None
        )
        db = query_df(conn, pub_title, pub_tag)
    magazines = glob.glob(os.path.join(magazine_path, "*"))
    mag_dir_name = magazine_path.split("/")[-1]
    magazines = [mag for mag in magazines if mag != mag_dir_name]

    for mag in magazines:
        meta_xml_path = glob.glob(os.path.join(mag, "*meta.xml"))[0]
        meta_xml = ET.parse(meta_xml_path)

        try:
            # may raise Exception when date is not parseable
            # e.g. 'fourteenth of february' is not parseable
            issue_date = extract_date(meta_xml)
        except Exception as e:
            print(f"Could not extract date for {mag}")
            continue

        # fitler after date and sort after the page number of stories
        filter = db[
            (db["issue_year"] == issue_date.year)
            & (db["issue_month"] == issue_date.month)
        ]
        filter = filter.sort_values(by=["pubc_page"])
        if len(filter) == 0:
            # print(f"No matches for {mag} with extracted date {issue_date.year}-{issue_date.month}")
            continue

        # sometimes the xml does not contain ids or the ids are not parseable for whatever reason
        try:
            mag_id = extract_id(meta_xml)
        except Exception as e:
            print(f"Could not extract id from xml '{meta_xml_path}'", e)
            continue

        # just take the very first attributes, because the query is aggregated
        xml = extract(issue_date, filter, mag_id)
        sub_element_text(xml, "magazine-query", f"title:{pub_title}|tag:{pub_tag}")
        ET.indent(xml)
        xml = ET.tostring(xml)
        with open(os.path.join(mag, "metadata.xml"), "wb") as f:
            f.write(xml)


def sanity_check(mag_dir, xml: ET.Element):
    mag_txt = glob.glob(mag_dir + "/*.txt")[0]
    with open(mag_txt, "r") as f:
        mag_txt = f.read().lower()

    matches = 0
    no_matches = 0
    entries = xml.find("toc").findall("toc-entry")
    for entry in entries:
        if entry.text.lower() in mag_txt:
            matches += 1
        else:
            no_matches += 1

    return matches, no_matches


@app.command()
def extract_all():
    """
    Extracts and saves meta data.\n
        - Authentic Science-Fiction\n
        - Asimov Magazine\n
        - IF Magazine\n
        - Astounding Stories\n
        - Galaxy Magazine
    """
    # process(CONN, "corpus/authenticsciencefiction", "Authentic Science Fiction")
    # process(CONN, "corpus/asimovmagazine", "Asimov's Science Fiction")
    # process(
    #     CONN, "corpus/ifmagazine", pub_tag="WOFI"
    # )  # problem: some magazines are published between two dates and isfdb db chooses the minor date, archive chooses the major date. Only for magazines starting from 1971
    process(CONN, "corpus/astoundingstories", pub_tag="AST")

    process(CONN, "corpus/galaxymagazine", pub_title="Galaxy")


@app.command()
def missing_metadata(corpus_path: Path):
    """Prints missing metadata xmls."""
    for mag in corpus_path.iterdir():
        total = 0
        for issue in mag.iterdir():
            if not issue.joinpath("metadata.xml").exists():
                total += 1
                print(f"No metadata.xml for {issue}")

        print(f"Total missing: {total}")
        print()


@app.command()
def remove_metadata(corpus_path: str):
    """Deletes metadata xmls."""
    metas = glob.glob("**/*/metadata.xml", recursive=True)
    for meta in metas:
        os.remove(meta)

    print(f"Deleted {len(metas)} metadata.xml")


def sanity_check_dir(score_thresh: float, len_thresh: float, issue_dir: Path):
    """Checks whether the given {issue_dir} has a valid metadata.xml."""
    metadata = issue_dir.joinpath("metadata.xml")
    if not metadata.exists():
        # print("No existing metadata.xml found")
        return

    txt_data = next(issue_dir.glob("*djvu.xml"))
    if txt_data is None or not txt_data.exists():
        # print("No existing *djvu.xml found")
        return

    txt_data = ET.parse(txt_data)
    txt_lines = txt_data.findall(
        "BODY/OBJECT/HIDDENTEXT/PAGECOLUMN/REGION/PARAGRAPH/LINE"
    )
    txt_words = [line.findall("WORD") for line in txt_lines]
    txt_words = [
        " ".join([word.text.lower() for word in word_line]) for word_line in txt_words
    ]

    xml = ET.parse(metadata)
    toc_entries = xml.findall("toc/toc-entry")
    total_matches = 0
    reviews = len(xml.findall("toc/toc-entry[@type='REVIEW']"))
    poems = len(xml.findall("toc/toc-entry[@type='POEM']"))

    result = {
        "issue_path": str(issue_dir),
        "total_matches": 0,
        "total_entries": len(toc_entries) - reviews - poems,
        "accuracy": 0.0,
    }

    for entry in toc_entries:
        title = entry.text.lower()
        for line_words in txt_words:
            score = fuzz.partial_ratio(title, line_words)
            dlen = len(line_words.replace(" ", "")) - len(title.replace(" ", ""))
            if score > score_thresh and dlen > -6:
                total_matches += 1
                break  # maybe count all?

    result["total_matches"] = total_matches
    result["accuracy"] = total_matches / result["total_entries"]
    if result["total_matches"] < 3:
        print(
            f"{issue_dir} maybe false matching. Only {total_matches} of {result['total_entries'] } were found."
        )

    return result


@app.command()
def sanity_check(
    issue_path: Path,
    single: bool = False,
    score_thresh: int = 75,
    len_thresh: float = 0.2,
):
    """
    Do sanity checking by partially string matching of titles.\n
    First, all lines of the djvu.xml files are extracted.
    After that extract the titles of the toc-entries from metadata.xml.

    Then a partial string matching is done by matching the titles from toc-entries with the lines from the djvu.xml files.
    Only accept matches that are maximally 20% longer or shorter and have a score greater than 75 (min=0, max=100).
    """

    if single:
        issue_paths = [issue_path]
    else:
        issue_paths = [x for x in issue_path.glob("*")]

    p = partial(sanity_check_dir, score_thresh, len_thresh)
    with Pool() as pool:
        results = pool.map(p, issue_paths)

    for result in results:
        if result is not None:
            out_path = Path(result["issue_path"]).joinpath("validation.json")
            with open(out_path, "w") as f:
                json.dump(result, f, ensure_ascii=False, indent=4)


@app.command()
def eval(magazine_path: Path, corpus: bool = False):
    if corpus:
        to_validate = [m for m in magazine_path.glob("*")]
    else:
        to_validate = [magazine_path]

    err_summary = {"no_matches": [], "mismatched": []}
    for magazine_path in to_validate:
        total_entries = 0
        total_matches = 0
        no_matches = 0

        issues = [x for x in magazine_path.glob("*")]
        for issue in issues:
            validation_json = issue.joinpath("validation.json")
            if not validation_json.exists():
                err_summary["no_matches"].append(str(issue))
                no_matches += 1
            else:
                with open(validation_json, "r") as f:
                    data = json.load(f)
                    total_entries += data["total_entries"]
                    total_matches += data["total_matches"]
                    if data["total_matches"] < 3:
                        err_summary["mismatched"].append(str(issue))

        total_issues = len(issues)
        issues_with_metadata = len(
            [x for x in issues if x.joinpath("metadata.xml").exists()]
        )
        issues_no_metadata = total_issues - issues_with_metadata
        mismatched_issues = len(err_summary["mismatched"])

        print("Evaluation: ", magazine_path)

        print("Issue-Level")
        print("Total issues", total_issues)
        print(
            "Number of issues without metadata",
            issues_no_metadata,
            f"({round(issues_no_metadata / total_issues, 4)})%",
        )
        print(
            "Number of issues with metadata",
            issues_with_metadata,
            f"({round(issues_with_metadata / total_issues, 4)})%",
        )
        print(
            "Number of issues with mismatched metadata",
            mismatched_issues,
            f"({round(mismatched_issues / issues_with_metadata, 4)})%",
        )
        print()

        print("TOC-Level")
        print("Total toc-entries", total_entries)
        print("Total toc-entries matched", total_matches)
        print("Detected mismatches", len(err_summary["mismatched"]))
        print("Accuracy", round(total_matches / total_entries, 4))

        print()

        with open(magazine_path.name + ".json", "w") as f:
            json.dump(err_summary, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    app()
