import sqlite3
from word2number import w2n
import pandas as pd

DATABASE = "isfdb/isfdb.db"
CONN = sqlite3.connect("isfdb/isfdb.db")



def build_query(pub_titles = None, pub_tag = None):
    where = ""
    if pub_titles is not None:
        if type(pub_titles) is list or type(pub_titles) is tuple:
            pub_titles = "%".join(pub_titles)

        where = f'WHERE p.pub_title  LIKE "%{pub_titles}%"'

    elif pub_tag is not None:
        where = f"WHERE p.pub_tag LIKE '{pub_tag}%'"

    return f'''
	SELECT pub_title, title_title, pub_year,pub_pages, publisher_name, pubc_page,title_copyright,title_ttype, pub_auth.author_legalname as editor_name, GROUP_CONCAT(a.author_legalname) as author_name, pub_frontimage, t.title_language as lang
	FROM pubs p
		RIGHT JOIN pub_content pc using (pub_id)
		LEFT JOIN titles t using (title_id)
		LEFT JOIN canonical_author ca using (title_id)
		LEFT JOIN authors a using(author_id)
		LEFT JOIN publishers  using(publisher_id)
		
		LEFT JOIN pub_authors pa on p.pub_id = pa.pub_id
		LEFT JOIN authors pub_auth on pa.author_id  = pub_auth.author_id
		{where}
		AND title_ttype != "INTERIORART"
		AND title_ttype != "COVERART"
		AND title_ttype != "EDITOR"
        AND lang == 17
	GROUP BY pub_title, title_title, pubc_page
	ORDER BY title_title, pubc_page;
	'''


def clean_pubc_page(row):
    pubc_page = str(row["pubc_page"])
    try:
        if "|" in pubc_page:
            page = pubc_page.split("|")
            if len(page[0]) > 0:
                return int(page[0])
            else:
                return int(page[1])
        if pubc_page is None or len(pubc_page) == 0 or pubc_page.lower() == "nan" or pubc_page.lower() == "none":
            return None
        else:
            try:
                return int(pubc_page)
            except:
                try:
                    return int(w2n.word_to_num(pubc_page))
                except:
                    return None
    except:
        return None

def query_df(conn, pub_title = None, pub_tag = None):
    query = build_query(pub_title, pub_tag)
    df = pd.read_sql_query(query, conn)
    df["pubc_page"] = df.apply(clean_pubc_page, axis=1)
    df["issue_year"] = df.apply(
        lambda x: x["pub_year"].split("-")[0], axis=1).astype(int)
    df["issue_month"] = df.apply(
        lambda x: x["pub_year"].split("-")[1], axis=1).astype(int)

    return df