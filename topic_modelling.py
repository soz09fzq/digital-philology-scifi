import pandas as pd
import os, sys, re
import codecs
from top2vec import Top2Vec  #  only versions >=3.7,<3.11 are supported

BASE_PATH = "D:\\stories_with_metadata\\stories\\"

def read_text_file(file_path):
    with codecs.open(file_path, 'r', 'utf-8') as f:
        raw_text = f.read()
        raw_text = raw_text.replace("-\n", "").replace("\n", " ")
        return raw_text
    

def read_texts(path):
    docs = []
    for root, dirs, files in os.walk(path):
        for dirname in dirs:
            if os.path.join(root, dirname).endswith('webscraping'):
                for file in os.listdir(os.path.join(root, dirname)):
                    if file.endswith(".txt"):
                        docs.append(read_text_file(f"{os.path.join(root, dirname)}\{file}"))

    return docs


if __name__ == "__main__":
    corpus = read_texts(BASE_PATH)
    model = Top2Vec(corpus[:3000])
    print(model.get_topic_sizes())
    topic_words, word_scores, topic_nums = model.get_topics(3)
    for words, scores, num in zip(topic_words, word_scores, topic_nums):
        print(num)
        print(f"Words: {words}")
        model.generate_topic_wordcloud(num)



    document, document_scores, document_ids = model.search_documents_by_topic(topic_num=1, num_docs=10)

    # for doc, score, doc_id in zip(document, document_scores, document_ids):
    #     print(f"Document: {doc_id}, Score: {score}")
    #     print("-----------------------------------")
    #     print(doc)
    #     print("------------------------------------\n")



